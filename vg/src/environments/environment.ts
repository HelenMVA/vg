// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC4eEpPrr0WDy6HHrfWQHuCkgfvd70W7qw",
    authDomain: "vika-4e079.firebaseapp.com",
    databaseURL: "https://vika-4e079.firebaseio.com",
    projectId: "vika-4e079",
    storageBucket: "vika-4e079.appspot.com",
    messagingSenderId: "77006652568",
    appId: "1:77006652568:web:2edc90be7cc9d8f845bced",
    measurementId: "G-TQQD16H72J"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
