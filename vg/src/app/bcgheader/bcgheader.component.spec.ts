import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BcgheaderComponent } from './bcgheader.component';

describe('BcgheaderComponent', () => {
  let component: BcgheaderComponent;
  let fixture: ComponentFixture<BcgheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BcgheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BcgheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
