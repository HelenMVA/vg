import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-user-account",
  templateUrl: "./user-account.component.html",
  styleUrls: ["./user-account.component.scss"]
})
export class UserAccountComponent implements OnInit {
  user: firebase.User;

  constructor(private router: Router, private auth: AuthService) {}
  ngOnInit() {
    this.auth.getUserState().subscribe(user => {
      this.user = user;
    });
  }
  isShow1 = false;
  isShow2 = true;
  isShow3 = false;
  myDisplay1() {
    this.isShow1 = true;
    this.isShow2 = false;
    this.isShow3 = false;
  }
  myDisplay2() {
    this.isShow1 = false;
    this.isShow2 = true;
    this.isShow3 = false;
  }
  myDisplay3() {
    this.isShow1 = false;
    this.isShow2 = false;
    this.isShow3 = true;
  }

  logout() {
    this.auth.logout();
  }
}
