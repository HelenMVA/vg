import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViktoriiaComponent } from './viktoriia.component';

describe('ViktoriiaComponent', () => {
  let component: ViktoriiaComponent;
  let fixture: ComponentFixture<ViktoriiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViktoriiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViktoriiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
