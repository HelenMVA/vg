import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"]
})
export class NavComponent implements OnInit {
  user: firebase.User;

  constructor(private router: Router, private auth: AuthService) {}

  ngOnInit() {
    this.auth.getUserState().subscribe(user => {
      this.user = user;
    });
  }
  login() {
    this.router.navigate(["/account"]);
  }
  // logout() {
  //   this.auth.logout();
  // }
  register() {
    this.router.navigate(["/account"]);
  }
}
