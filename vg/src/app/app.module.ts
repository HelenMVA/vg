import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { HeaderComponent } from "./header/header.component";
import { NavComponent } from "./nav/nav.component";
import { CategorieComponent } from "./categorie/categorie.component";
import { Section1Component } from "./section1/section1.component";
import { Section2Component } from "./section2/section2.component";
import { BcgheaderComponent } from "./bcgheader/bcgheader.component";
import { FooterComponent } from "./footer/footer.component";
import { MyAccountComponent } from "./Account/my-account/my-account.component";
import { MainComponent } from "./main/main.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule } from "@angular/material/button";
import { RegisterComponent } from "./Account/register/register.component";
import { LoginComponent } from "./Account/login/login.component";
import { environment } from "src/environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ViktoriiaComponent } from "./Explore/viktoriia/viktoriia.component";
import { TopsComponent } from "./shop/tops/tops.component";
import { AccountDetailsComponent } from "./userSpace/account-details/account-details.component";
import { FavoritesComponent } from "./userSpace/favorites/favorites.component";
import { OrderHistoryComponent } from "./userSpace/order-history/order-history.component";
import { UserAccountComponent } from "./userSpace/user-account/user-account.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    NavComponent,
    LoginComponent,
    CategorieComponent,
    Section1Component,
    Section2Component,
    BcgheaderComponent,
    FooterComponent,
    MyAccountComponent,
    MainComponent,
    RegisterComponent,
    ViktoriiaComponent,
    TopsComponent,
    AccountDetailsComponent,
    FavoritesComponent,
    OrderHistoryComponent,
    UserAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, "vg"),
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
