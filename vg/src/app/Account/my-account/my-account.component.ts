import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-my-account",
  templateUrl: "./my-account.component.html",
  styleUrls: ["./my-account.component.scss"]
})
export class MyAccountComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
  isShow = true;
  isShow2 = false;

  toggleDisplay() {
    // this.isShow = !this.isShow;
    this.isShow = true;
    this.isShow2 = false;
  }
  toggleDisplay2() {
    // this.isShow = !this.isShow;
    this.isShow2 = true;
    this.isShow = false;
  }
}
