import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  authError: any;

  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit() {
    this.auth.eventAutherror$.subscribe(data => {
      this.authError = data;
    });
  }

  createUser(frm) {
    this.auth.createUserServ(frm.value);
    // this.router.navigate(["/"]);
  }
}
