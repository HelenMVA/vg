import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { MyAccountComponent } from "./Account/my-account/my-account.component";
import { CategorieComponent } from "./categorie/categorie.component";
import { ViktoriiaComponent } from "./Explore/viktoriia/viktoriia.component";
import { TopsComponent } from "./shop/tops/tops.component";
import { UserAccountComponent } from "./userSpace/user-account/user-account.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "account", component: MyAccountComponent },
  { path: "test", component: CategorieComponent },
  { path: "Viktiriia", component: ViktoriiaComponent },
  { path: "Tops", component: TopsComponent },
  { path: "UserSpace", component: UserAccountComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
