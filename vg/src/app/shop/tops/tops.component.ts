import { Component, OnInit } from "@angular/core";
import { TopsService } from "../../services/tops.service";
import { Top } from "../../models/Top";

@Component({
  selector: "app-tops",
  templateUrl: "./tops.component.html",
  styleUrls: ["./tops.component.scss"]
})
export class TopsComponent implements OnInit {
  tops: Top[];
  constructor(private topsService: TopsService) {}

  ngOnInit() {
    this.topsService.getTops().subscribe(tops => {
      this.tops = tops;
    });
  }
}
