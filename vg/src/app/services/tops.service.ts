import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { Top } from "../models/Top";

@Injectable({
  providedIn: "root"
})
export class TopsService {
  topsCollection: AngularFirestoreCollection<Top>;
  tops: Observable<Top[]>;
  constructor(public afs: AngularFirestore) {
    this.tops = this.afs.collection("Tops").valueChanges();
  }
  getTops() {
    return this.tops;
  }
}
